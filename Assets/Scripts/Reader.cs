﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reader : MonoBehaviour
{
    // Start is called before the first frame update

    public string Message;
    private Light luz;
    private GameObject Player;
    void Start()
    {
        Message = this.GetComponent<Interactable>().Message;
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Var"))
        {
            this.GetComponent<Interactable>().Message = "O valor da Variável é '"+ other.gameObject.name.Substring(4)+"'";
            //Debug.Log(other.gameObject.name.Substring(4));
            luz.intensity = 10;
            if(Player.GetComponent<PlayerMovement>().Message){
                Player.GetComponent<PlayerMovement>().Message.text = this.GetComponent<Interactable>().Message;
            }
            Player.transform.GetChild(10).gameObject.SetActive(true);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Var"))
        {
            luz.intensity = 0;
            this.GetComponent<Interactable>().Message = Message;
            Player.transform.GetChild(10).gameObject.SetActive(false);
        }
    }
}
