﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conditional : MonoBehaviour
{
    public string value1;
	public string op;
	public string value2;
	private bool result;
    
    // Start is called before the first frame update
    void Start()
    {
        //result = false;
        //get value1
        //value1 = "num:0";
        //get value2
        //value2 = "num:0";
        //get operator
        //op = "=";
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void checkVar(){
        string type1 = value1.Substring(0, 3);
        if(type1 == "var"){
            string varname = value1.Substring(4, 7);
            
        }
    }

    string getType(){
        string type1 = value1.Substring(0, 3);
        string type2 = value2.Substring(0, 3);
        if(type1 == type2){
            //calcula
            if(type1 == "num"){
                //calcula num
                return "num";
            }else{
                if(type1 == "str"){
                    //calcula str
                    return "str";
                }else{
                    if(type1 == "bol"){
                        //calcula bol
                        return "bol";
                    }else{
                        return "err";
                    }
                }
            }
        }else{
            return "err";
        }
    }

    public bool CalculateNum(){
        string val1 = value1.Substring(2, 5);
        string val2 = value2.Substring(2, 5);
        int calcValue1 = int.Parse(val1);
        int calcValue2 = int.Parse(val2);
        if(op == "="){
            return (calcValue1==calcValue2);    
        }else{
            if(op == ">"){
                return (calcValue1>calcValue2);
            }else{
                if(op=="<"){
                    return (calcValue1<calcValue2);
                }else{
                    return false;
                }
            }
        }
    }
}
