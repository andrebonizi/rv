﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	public static string lastScene = "Intro";

	
	public static void Load(string scene){
		lastScene = scene;
		SceneManager.LoadScene(scene, LoadSceneMode.Single);
		//Time.timeScale = 1;
	}

	public static void LoadNextScene(){
		Scene scene = SceneManager.GetActiveScene();
		lastScene = scene.name;
		int nextScene = scene.buildIndex +1;
		SceneManager.LoadScene(nextScene, LoadSceneMode.Single);
	}
	public static void LoadPrevScene(){
		Scene scene = SceneManager.GetActiveScene();
		lastScene = scene.name;
		int nextScene = scene.buildIndex -1;
		SceneManager.LoadScene(nextScene, LoadSceneMode.Single);
	}
}
