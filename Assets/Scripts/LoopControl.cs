﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LoopControl : MonoBehaviour
{
    private string op;
    private string type;
    private bool result;
    private Light luz;
    //public GameObject choque;
    
    private SceneLoader sceneLoader;
    private GameObject pullers;
    private GameObject var;
    private GameObject roof;
    
    private GameObject doorELSE;
    private int count;
    
    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = GetComponent<SceneLoader>();
        result = false;
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        pullers = this.gameObject.transform.GetChild(0).gameObject;
        op = this.gameObject.name.Substring(7, 2);
        type = this.gameObject.name.Substring(4, 3);
    }

    void Update(){
        if(var){
            Debug.Log(roof.activeInHierarchy);
            if(var.transform.hasChanged){
                
                roof.SetActive(true);
                var.transform.hasChanged = false;
            }else{
                roof.SetActive(false);
            }
        }
        
            
    }
    void OnTriggerEnter(Collider other){
        
        if (other.gameObject.CompareTag("Var") && PlayerMovement.Life > 0)
        {
            var = other.gameObject;
            roof = this.transform.GetChild(8).gameObject;
            roof.SetActive(true);
            if (other.gameObject.name.Substring(0,3) != type)
            {
                PlayerMovement.Life -= 1;
                //choque.SetActive(true);
                this.GetComponent<AudioSource>().Play();
                if(PlayerMovement.Life <= 0){
                    GameObject.Find("Player").GetComponent<Animator>().SetTrigger("Die");
                    //SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
                    pullers.SetActive(false);
                }
            }
            
        }
        if (other.gameObject.CompareTag("Var") && other.gameObject.name.Substring(0,3) == type)
        {
            
            
            bool check;
            switch(op){
                case "==":check = other.gameObject.name.Substring(4) == this.gameObject.name.Substring(9);break;
                case "!=": check = (other.gameObject.name.Substring(4) != this.gameObject.name.Substring(9));break;
                case ">=": check = (int.Parse(other.gameObject.name.Substring(4)) >= int.Parse(this.gameObject.name.Substring(9)));break;
                case "<=": check = ((int.Parse(other.gameObject.name.Substring(4)) <= int.Parse(this.gameObject.name.Substring(9))));break;
                default:check = false;break;
            }
            if(!check){
                if(other.name.Substring(3,1) == "_"){
                    other.transform.Translate(2, 0,0);
                    
                }else{
                    other.transform.Translate(0,2,0);
                }
            }
            validate(check);
        }
    }
    private void validate(bool expression){
        if(expression){
            openDoor();
        }else{
            openDoorELSE();
        } 
    }
    private void openDoor(){
        luz.color = Color.green;
        luz.intensity = 10;
        result = true;
        pullers.SetActive(true);

    }
    private void openDoorELSE(){
        luz.color = Color.red;
        luz.intensity = 10;
        result = false;
        pullers.SetActive(false);
    }
    private void closeDoors(){
        luz.intensity = 0;
        result = false;
        pullers.SetActive(false);
        this.GetComponent<AudioSource>().Stop();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Var"))
        {
            
            //closeDoors();
            pullers.SetActive(true);
            
            //this.transform.GetChild(8).gameObject.SetActive(false);
        }
    }
}
