﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Operator : MonoBehaviour
{
    private Light luz;
    private string opr;
    public bool enable = false;
    public int carga = -1;
    // Start is called before the first frame update
    void Start()
    {
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        opr = this.gameObject.name.Substring(3,1);
        switch(opr){
            case "=":
                this.gameObject.GetComponent<Interactable>().Message = "VAR = "+this.gameObject.name.Substring(4);
            break;
            case "+":
                this.gameObject.GetComponent<Interactable>().Message = "VAR = VAR + "+this.gameObject.name.Substring(4);
            break;
            case "-":
                this.gameObject.GetComponent<Interactable>().Message = "VAR = VAR - "+this.gameObject.name.Substring(4);
            break;
            case "*":
                this.gameObject.GetComponent<Interactable>().Message = "VAR = VAR * "+this.gameObject.name.Substring(4);
            break;
        }            
    }

    

    private void parseIntResult(Collision other, int result){
        if(result >= 0){
            switch(result.ToString().Length){
                case 1: other.gameObject.name = other.gameObject.name.Substring(0,4)+"00"+result.ToString();break;
                case 2: other.gameObject.name = other.gameObject.name.Substring(0,4)+"0"+result.ToString();break;
                default:other.gameObject.name = other.gameObject.name.Substring(0,4)+result.ToString();break;
            }
        }else{
            switch(result.ToString().Length){
                case 2: other.gameObject.name = other.gameObject.name.Substring(0,4)+"-0"+result.ToString().Substring(1);break;
                default:other.gameObject.name = other.gameObject.name.Substring(0,4)+"-"+result.ToString().Substring(1);break;
            }
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Var") && carga != 0)
        {
            this.enable = true;
            luz.intensity = 100;
            carga = carga > 0? carga-1 : carga;
            int result = 0;
            string word = "";
            //tem que ocorrer no Player
            if(enable){
                opr = this.gameObject.name.Substring(3,1);
                if(this.GetComponent<AudioSource>()){
                    this.GetComponent<AudioSource>().Play();
                }
                switch(opr){
                    case "=":
                        other.gameObject.name = other.gameObject.name.Substring(0,4) + this.gameObject.name.Substring(4);
                    break;
                     
                    case "+":
                        result = int.Parse(other.gameObject.name.Substring(4))+int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "-":
                        result = int.Parse(other.gameObject.name.Substring(4))-int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "*":
                        result = int.Parse(other.gameObject.name.Substring(4))*int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "/":
                        if(int.Parse(this.gameObject.name.Substring(4)) != 0){
                            result = int.Parse(other.gameObject.name.Substring(4))/int.Parse(this.gameObject.name.Substring(4));
                        }else{
                            result = 0;
                        }
                    break;
                }
                if(opr != "="){
                    parseIntResult(other, result);
                }
                if(other.gameObject.name.Substring(0,3) == "WRD"){
                    other.gameObject.name = other.gameObject.name.Substring(0,4) + word;
                }
                enable = false;
            }
            
        }


    }


    void OnCollisionExit(Collision other){
        if(other.gameObject.CompareTag("Var"))
        {
            this.enable = false;
            luz.intensity = 0;

        }
    }

    
}
