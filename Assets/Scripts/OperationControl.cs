﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperationControl : MonoBehaviour
{
    private Light luz;
    private string opr;
    private string thisType;
    private string otherType;
    
    public bool enable = false;
    public int carga = -1;
    // Start is called before the first frame update
    void Start()
    {
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        opr = this.gameObject.name.Substring(3,1);
        thisType = this.gameObject.name.Substring(0,3);
        switch(opr){
            case "=":
                this.gameObject.GetComponent<Interactable>().Message = "VAL = "+this.gameObject.name.Substring(4);
            break;
            case "+":
                this.gameObject.GetComponent<Interactable>().Message = "VAL = VAL + "+this.gameObject.name.Substring(4);
            break;
            case "-":
                this.gameObject.GetComponent<Interactable>().Message = "VAL = VAL - "+this.gameObject.name.Substring(4);
            break;
            case "*":
                this.gameObject.GetComponent<Interactable>().Message = "VAL = VAL * "+this.gameObject.name.Substring(4);
            break;
        }            
    }

    
    
    private string calcString(GameObject otherObj){
        string result = "";
        string thisVal = this.gameObject.name.Substring(4);
        string otherVal = otherObj.name.Substring(4);
        switch(opr){
            case "=":result = thisVal;break;
            case "+":result = otherVal + thisVal;break;
            default:result = "error";break;
        }
        return result;
    }

    private int calcInt(GameObject otherObj){
        int result = 0;
        int thisVal = int.Parse(this.gameObject.name.Substring(4));
        int otherVal = 0;
        if(opr != "="){
            otherVal = int.Parse(otherObj.name.Substring(4));
        }
        switch(opr){
            case "=":result = thisVal;break;
            case "+":result = otherVal + thisVal;break;
            case "-":result = otherVal - thisVal;break;
            case "*":result = otherVal * thisVal;break;
            case "/": result = otherVal /(thisVal != 0 ? thisVal : 1);break;
            default:result = 0;break;
        }
        return result;
    }

    private bool calcBol(GameObject otherObj){
        bool result = false;
        bool thisVal = this.gameObject.name.Substring(4) == "TRUE" ? true : false;
        bool otherVal = otherObj.name.Substring(4) == "TRUE" ? true : false;
        switch(opr){
            case "=":result = thisVal;break;
            default:result = false;break;
        }
        return result;
    }
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Var") && carga != 0){
            this.enable = true;
            luz.intensity = 100;
            carga = carga > 0? carga-1 : carga;
            if(this.GetComponent<AudioSource>()){
                this.GetComponent<AudioSource>().Play();
            }
            otherType = other.gameObject.name.Substring(0,3);
            switch(otherType){
                case "NUM":
                    other.gameObject.name = otherType + ":" + calcInt(other.gameObject).ToString();
                break;
                case "STR":
                    if(this.gameObject.name.Substring(4) != "TRUE" && this.gameObject.name.Substring(4) != "FALSE"){
                        other.gameObject.name = otherType + ":" + calcString(other.gameObject);
                    }else{
                        other.gameObject.name = otherType + ":" + "ERROR";
                    }
                    
                break;
                case "BOL":
                    if(calcBol(other.gameObject)){
                        other.gameObject.name = otherType + ":" + "TRUE";
                    }else{
                        other.gameObject.name = otherType + ":" + "FALSE";
                    } 
                break;
                case "VAR":
                    other.gameObject.name = "ERR:ERROR";
                break;
                default:
                    Debug.Log("error");
                    other.gameObject.name = "ERR:ERROR";
                break;
            }
        }
        
        

        


        /* antigamente eu fiz assim, tá errado eu sei

        if (other.gameObject.CompareTag("Var") && carga != 0)
        {
            this.enable = true;
            luz.intensity = 100;
            carga = carga > 0? carga-1 : carga;
            int result = 0;
            string word = "";
            //tem que ocorrer no Player
            if(enable){
                opr = this.gameObject.name.Substring(3,1);
                if(this.GetComponent<AudioSource>()){
                    this.GetComponent<AudioSource>().Play();
                }
                switch(opr){
                    case "=":
                        other.gameObject.name = other.gameObject.name.Substring(0,4) + this.gameObject.name.Substring(4);
                    break;
                     
                    case "+":
                        result = int.Parse(other.gameObject.name.Substring(4))+int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "-":
                        result = int.Parse(other.gameObject.name.Substring(4))-int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "*":
                        result = int.Parse(other.gameObject.name.Substring(4))*int.Parse(this.gameObject.name.Substring(4));
                    break;
                    case "/":
                        if(int.Parse(this.gameObject.name.Substring(4)) != 0){
                            result = int.Parse(other.gameObject.name.Substring(4))/int.Parse(this.gameObject.name.Substring(4));
                        }else{
                            result = 0;
                        }
                    break;
                }
                if(opr != "="){
                    //parseIntResult(other, result);
                }
                if(other.gameObject.name.Substring(0,3) == "WRD"){
                    other.gameObject.name = other.gameObject.name.Substring(0,4) + word;
                }
                enable = false;
            }
            
        }
        */

    }


    void OnCollisionExit(Collision other){
        if(other.gameObject.CompareTag("Var"))
        {
            this.enable = false;
            luz.intensity = 0;

        }
    }

    /*
    private void parseIntResult(Collision other, int result){
        if(result >= 0){
            switch(result.ToString().Length){
                case 1: other.gameObject.name = other.gameObject.name.Substring(0,4)+"00"+result.ToString();break;
                case 2: other.gameObject.name = other.gameObject.name.Substring(0,4)+"0"+result.ToString();break;
                default:other.gameObject.name = other.gameObject.name.Substring(0,4)+result.ToString();break;
            }
        }else{
            switch(result.ToString().Length){
                case 2: other.gameObject.name = other.gameObject.name.Substring(0,4)+"-0"+result.ToString().Substring(1);break;
                default:other.gameObject.name = other.gameObject.name.Substring(0,4)+"-"+result.ToString().Substring(1);break;
            }
        }
    }
    */
}
