using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    private Light luz;
    private string dir;
    public GameObject robot;
    private Transform robotTransform;
    private Animator robotAnimator;
    
    public bool enable = false;
    public int carga = -1;
    void Start()
    {
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        dir = this.gameObject.name.Substring(4);
        //thisType = this.gameObject.name.Substring(0,3); 
        robotAnimator = robot.GetComponent<Animator>();
        robotTransform = robot.GetComponent<Transform>();        
    }

    void FixedUpdate(){
        if(luz.intensity > 0){
            robotAnimator.SetBool("IsRunning", true);
        }else{
            robotAnimator.SetBool("IsRunning", false);
        }
    }
    void move(){
        Debug.Log(dir);
        switch(dir){
            case "up":
                robotTransform.eulerAngles = new Vector3(0f, 0f, 0f);
                //robotTransform.Translate(0,0,.1f);
            break;
            case "down":
                robotTransform.eulerAngles = new Vector3(0f, 180f, 0f);
                //robotTransform.Translate(0,0,-.1f);
            break;
            case "left":
                robotTransform.eulerAngles = new Vector3(0f, -90f, 0f);
                //robotTransform.Translate(-.1f,0,0);
                
            break;
            case "right":
                robotTransform.eulerAngles = new Vector3(0f, 90f, 0f);
                //robotTransform.Translate(.1f,0,0);
                
                
            break;
        }
        
        robotTransform.Translate(0,0,.1f);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Var") && carga != 0){
            this.enable = true;
            luz.intensity = 100;
            carga = carga > 0? carga-1 : carga;
            if(this.GetComponent<AudioSource>()){
                this.GetComponent<AudioSource>().Play();
            }
            move();
            
        }
    }


    void OnCollisionExit(Collision other){
        if(other.gameObject.CompareTag("Var"))
        {
            this.enable = false;
            luz.intensity = 0;
            if(this.GetComponent<AudioSource>()){
                this.GetComponent<AudioSource>().Stop();
            }

        }
    }
}
