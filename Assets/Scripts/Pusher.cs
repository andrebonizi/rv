﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pusher : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other){
        //Debug.Log(other.gameObject.name);
        if(other.gameObject.CompareTag("Var")){
            other.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            Vector3 dir;
            dir = transform.position - other.transform.position;
            //Debug.Log(dir);
            other.GetComponent<Rigidbody>().AddForce(new Vector3(dir.x,0f,dir.z)*100, ForceMode.Impulse);
            GetComponent<Light>().intensity = 1;
            //Isso evita que o Player entre no LOOP
            // /PlayerMovement.enabled = false;
            
        }
    }

    void OnTriggerExit(Collider other){
        if(other.gameObject.CompareTag("Var")){
            GetComponent<Light>().intensity = 0;
            //other.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
            
        }
    }
}
