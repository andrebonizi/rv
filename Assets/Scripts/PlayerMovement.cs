﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {


	public static int Life = 10;
	public static int Gold = 0;
	
	//public Text txtLife;
	//private Text txtGold;
	//public static GameObject MessageBox;
	//GameObject Interact;
	
	
	public static string holding;

	private Animator anim;
	public static Vector3 pos;
	public Vector3 rot;
	private Rigidbody rigidbody;
	int floorMask;
	float camRayLength = 100f;
	public float h,v;
	private float speed;
	private bool walking;
	// Use this for initialization
	void Start () {
		//holding = "nada";
		floorMask = LayerMask.GetMask("Floor");
		anim = GetComponent<Animator>();
		rigidbody = GetComponent<Rigidbody>();
		pos = GetComponent<Transform>().position;
		
		if(EnemyController.battle < 1){
			transform.position = new Vector3(BattleController.position.x,transform.position.y,BattleController.position.z);
		}
		
		//MessageBox = GameObject.FindWithTag("ScreenCanvas").transform.GetChild(2).gameObject;
		//Interact = GameObject.FindWithTag("Interactor");
		
		//txtLife = GameObject.FindWithTag("Life").GetComponent<Text>();
		//txtGold = GameObject.FindWithTag("Gold").GetComponent<Text>();
		walking =false;
	}

	public static string GetHolding () {
		return holding;
	}
	
	public static void DropHolding(){
		holding = "nada";

	}

	public static void HoldItem (string item) {
		if(holding == "nada"){
			holding = item;
		}
	}

	// Update is called once per frame
	void Update () {
		speed=h+v;
		if(speed<0){
			speed *= -1; 
		}
		anim.SetFloat("Speed", speed);
	}

	void FixedUpdate(){
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		//txtLife.text = ""+Life;
		//txtGold.text = ""+Gold;
		if(Input.GetKeyDown(KeyCode.Space)){
			//MessageBox.SetActive(false);
			//Interact.SetActive(false);
		}
		if( h != 0 ){
			
			if(h > 0){
				if(v>0){transform.eulerAngles = new Vector3(0f,45f,0f);}
				else{transform.eulerAngles = new Vector3(0f,135f,0f);}
				if(v==0){transform.eulerAngles = new Vector3(0f,90f,0f);transform.Translate(0f,0f,h*.1f );}
				
				transform.Translate(0f,0f,h*.1f );
			}else{
				if(v>0){transform.eulerAngles = new Vector3(0f,-45f,0f);}
				else{transform.eulerAngles = new Vector3(0f,-135f,0f);}
				if(v==0){transform.eulerAngles = new Vector3(0f,-90f,0f);transform.Translate(0f,0f,-h*.1f );}
				
				transform.Translate(0f,0f,-h*.1f );
			}
		}
		if(v != 0){
			
			if(v>0){
				if(h>0){transform.eulerAngles = new Vector3(0f,45f,0f);}
				else{transform.eulerAngles = new Vector3(0f,-45f,0f);}
				if(h==0){transform.eulerAngles = new Vector3(0f,0f,0f);transform.Translate(0f,0f,v*.1f );}
				
				
				transform.Translate(0f,0f,v*.1f );
				
			}else{
				if(h>0){transform.eulerAngles = new Vector3(0f,135f,0f);}
				else{transform.eulerAngles = new Vector3(0f,225f,0f);}
				if(h==0){transform.eulerAngles = new Vector3(0f,180f,0f);transform.Translate(0f,0f,-v*.1f );}
				transform.Translate(0f,0f,-v*.1f );
				
			}
			
		}
		
	}
	

	
}
