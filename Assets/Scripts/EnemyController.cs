﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {
	// Use this for initialization
	GameObject player;
	public GameObject battleStart;
	Animator anim;
	public static int battle = 1;
	float distance;
	float moveSpeed;
	void Start () {
		player = GameObject.FindWithTag("Player");
		moveSpeed = 0.1f;
		anim = GetComponent<Animator>();
		if(battle <= 0){
			Destroy(this.gameObject);
			battle = 1;
		}
		
	}
	void Update(){
		if(player != null){
			distance = Vector3.Distance(player.transform.position,this.transform.position);	
			if(distance < 10f){
				transform.Translate(new Vector3(0,0,moveSpeed));
				transform.Rotate(new Vector3(0,0.2f,0));
				this.transform.LookAt(player.transform);
				
				if(moveSpeed<0.04f){
					moveSpeed += 0.01f;
					//anim.SetBool("IsRunning", true);
					
					anim.SetFloat("Speed", moveSpeed);
				}else{
					anim.SetFloat("Speed", moveSpeed);
					//anim.SetBool("IsRunning", true);
				}
			}else{
				//moveSpeed -= 0.01f;
				//anim.SetBool("IsRunning", false);
			}

		}

		
	}
	// Update is called once per frame
	

	void OnCollisionEnter(Collision other){
		if(other.gameObject.CompareTag("Player")){
			BattleController.position = other.transform.position;
			string lastScene = (string)SceneManager.GetActiveScene().name;
			SceneLoader.lastScene = lastScene;
			SceneManager.LoadScene("Battle", LoadSceneMode.Single);
			//GameObject battleStart = GameObject.FindWithTag("ScreenCanvas").transform.GetChild(3).gameObject;
			//battleStart.SetActive(true);
			//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = "Batalha!";
			//PlayerMovement.MessageBox.SetActive(true);
			Destroy(this.gameObject);
			
		}
	}
}
