﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConditionControl : MonoBehaviour
{
    private string op;
    private string type;
    private bool result;
    private Light luz;
    public GameObject choque;
    
    private SceneLoader sceneLoader;
    private GameObject door;
    private GameObject doorELSE;
    
    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = GetComponent<SceneLoader>();
        result = false;
        luz = this.GetComponentInChildren<Light>();
        luz.intensity = 0;
        door = this.gameObject.transform.GetChild(1).gameObject;
        doorELSE = this.gameObject.transform.GetChild(2).gameObject;
        if(doorELSE.name != "DoorELSE"){
            doorELSE = null;
        }
        choque.SetActive(false);
        op = this.gameObject.name.Substring(7, 2);
        type = this.gameObject.name.Substring(4, 3);
    }

    void OnCollisionEnter(Collision other){
        if (other.gameObject.CompareTag("Var"))
        {
            
            

            if (other.gameObject.name.Substring(0,3) != type)
            {
                Debug.Log(PlayerMovement.currentLevel.name);
                PlayerMovement.Life -= 1;
                Debug.Log(PlayerMovement.Life);
                choque.SetActive(true);
                this.GetComponent<AudioSource>().Play();
                if(PlayerMovement.Life <= 0){
                    GameObject.Find("Player").GetComponent<Animator>().SetTrigger("Die");

                    //SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
                }
            }
            
        }
    }
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Var") && other.gameObject.name.Substring(0,3) == type)
        {
            
            
            
            switch(op){
                
                case "==":
                    if (other.gameObject.name.Substring(4) == this.gameObject.name.Substring(9))
                    {
                        openDoor();
                    }else{
                        openDoorELSE();
                    }
                break;
                case "!=":
                    if (other.gameObject.name.Substring(4) != this.gameObject.name.Substring(9))
                    {
                        openDoor();
                    }else{
                        openDoorELSE();
                    }
                break;
                case ">=":
                    if (int.Parse(other.gameObject.name.Substring(4)) >= int.Parse(this.gameObject.name.Substring(9)))
                    {
                        openDoor();
                    }else{
                        openDoorELSE();
                    }
                break;
                case "<=":
                    if (int.Parse(other.gameObject.name.Substring(4)) <= int.Parse(this.gameObject.name.Substring(9)))
                    {
                        openDoor();
                    }else{
                        openDoorELSE();
                    }
                break;
            }
            
        }
    }
    private void openDoor(){
        luz.color = Color.green;
        luz.intensity = 10;
        result = true;
        door.SetActive(false);
        if(doorELSE != null){
            doorELSE.SetActive(true);
        }
        
        
    }
    private void openDoorELSE(){
        luz.color = Color.red;
        luz.intensity = 10;
        result = false;
        door.SetActive(true);
        if(doorELSE != null){
            doorELSE.SetActive(false);
        }
        
    }
    private void closeDoors(){
        luz.intensity = 0;
        result = false;
        door.SetActive(true);
        if(doorELSE != null){
            doorELSE.SetActive(true);
        }
        
        choque.SetActive(false);
        this.GetComponent<AudioSource>().Stop();
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Var"))
        {
            closeDoors();
        }
    }
}
