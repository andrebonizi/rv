﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class BattleController : MonoBehaviour {

	private string lastScene;


	//Battle Control Variables
	public static int hit;
	public GameObject options;
	public GameObject aware;
	public static Vector3 position;

	int modelNumber;
	public GameObject [] modelArray;
	public GameObject currentModel;
	
	private int HeroLife;
	private int HeroAtk;
	private int HeroSpd;
	
	private int EnLife;
	private int EnAtk;
	private int EnSpd;

	public Animator Hero;
	public Transform HeroPosition;
	public Animator Enemy;
	public Transform EnemyPosition;

	//Text Variables
	public Text text;

	public Text heDam;
	public Text heLifeTxt;
	
	public Text enDam;
	public Text enLifeTxt;
	
	//Animation Variables
	public int enumAnim;
	Vector3 auxPosHeroIn;
	Vector3 auxPosHeroAtk;
	Vector3 auxPosEnIn;
	Vector3 auxPosEnAtk;
	
	

	void Start () {
		//GameObject enemy = Instantiate(Resources.Load("Red Dragon", typeof(GameObject)) ) as GameObject;
		//Enemy = Resources.Load("Red Dragon", typeof(GameObject)) as Animator;
		position = new Vector3(-0.3261583f,-2.053f,-2.981557f);
		lastScene = SceneLoader.lastScene;
		
		currentModel = modelArray[0];
		EnLife = 2;
		EnAtk = 1;
		EnSpd = 1;

		HeroLife = PlayerMovement.Life;
		HeroAtk = 1;
		HeroSpd = 1;

		auxPosEnIn = EnemyPosition.position;
		auxPosEnAtk = HeroPosition.position+(new Vector3(-1,0,1));
		
		auxPosHeroIn = HeroPosition.position;
		auxPosHeroAtk = EnemyPosition.position+(new Vector3(1,0,-1));
		
		enumAnim = 0;
		StartCoroutine(Battle());
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			TimerHit.cont++;
		}
		text.text = "cont: "+TimerHit.cont+" hit: "+hit;
		heLifeTxt.text = ""+HeroLife;
		enLifeTxt.text = ""+EnLife;

		if(enumAnim==1){
			//Lerp
			
			//auxPosHero = EnemyPosition.position+(new Vector3(1,0,-1));
			HeroPosition.position = Vector3.Lerp(HeroPosition.position, auxPosHeroAtk, Time.deltaTime*2f);

		}
		if(enumAnim==2){
			//Lerp
			
			HeroPosition.position = Vector3.Lerp(HeroPosition.position, auxPosHeroIn, Time.deltaTime*3);
			
		}
		if(enumAnim==3){
			//Lerp
			
			//auxPosEn = HeroPosition.position+(new Vector3(-1,0,1));
			EnemyPosition.position = Vector3.Lerp(EnemyPosition.position, auxPosEnAtk, Time.deltaTime*2f);
		}
		if(enumAnim==4){
			//Lerp
			EnemyPosition.position = Vector3.Lerp(EnemyPosition.position, auxPosEnIn, Time.deltaTime*3);
		}
	}

	IEnumerator Battle()
    {
        while(true){
			enumAnim = 3;
			Enemy.SetBool("IsWalking", true);
			
			yield return new WaitForSeconds(1);
			
			
			//EnemyPosition.position  = Vector3.Lerp(EnemyPosition.position, atkPos, 0.05f );
			//yield return new WaitForSeconds(4);
			Enemy.SetTrigger("IsAttacking");
			//yield return new WaitForSeconds(1);
			//EnemyPosition.Translate(new Vector3(0,0,-3));
			//EnemyPosition.position  = Vector3.Lerp(atkPos,EnemyPosition.position- atkPos, 0.05f );
			
			
			TimerHit.cont = 0;
			hit = 0;
			yield return new WaitForSeconds(1);
			options.SetActive(true);
			yield return new WaitForSeconds(.3f);
			options.SetActive(false);
			heDam.gameObject.SetActive(true);
			Hero.SetTrigger("GetHit");
			enumAnim = 4;
			Enemy.SetBool("IsWalking", false);
			if(hit != 1){
				int damage = EnAtk+EnAtk;
				HeroLife -= damage;
				heDam.text = ""+damage;
			}else{
				HeroLife -= EnAtk;
				heDam.text = ""+EnAtk;
			}
			yield return new WaitForSeconds(.3f);
			heDam.gameObject.SetActive(false);
			if(HeroLife<=0){
				HeroLife = 0;
				break;
			}
			
			yield return new WaitForSeconds(1);
			//aware.SetActive(true);
			TimerHit.cont = 0;
			hit = 0;
			//auxPos = HeroPosition.position;

			HeroPosition.Rotate(0,-45,0);
			enumAnim = 1;
			Hero.SetBool("IsWalking", true);
			yield return new WaitForSeconds(1f);
			Hero.SetBool("IsWalking", false);
			options.SetActive(true);
			yield return new WaitForSeconds(.3f);
			options.SetActive(false);
			
			
			//aware.SetActive(false);
			
			if(hit == 1){
				Hero.SetTrigger("HeavyAttack");
				int damage = HeroAtk+HeroAtk;
				enDam.text = ""+damage;
				EnLife -= damage;
				
			}else{
				Hero.SetTrigger("NormalAttack");
				enDam.text = ""+HeroAtk;
				EnLife -= HeroAtk;
			}
			if(EnLife < 0){
				EnLife = 0;
			}
			Enemy.SetTrigger("GetHit");
			

			
			if(EnLife<=0){
				Enemy.SetTrigger("Die");
				//yield return new WaitForSeconds(5);
				break;
			}
			enDam.gameObject.SetActive(true);
			yield return new WaitForSeconds(.4f);
			enDam.gameObject.SetActive(false);
			yield return new WaitForSeconds(.8f);
			enumAnim = 2;
			HeroPosition.Rotate(0,180,0);
			Hero.SetBool("IsWalking", true);
			
			yield return new WaitForSeconds(1);
			Hero.SetBool("IsWalking", false);
			HeroPosition.Rotate(0,180,0);
			HeroPosition.Rotate(0,45,0);
		}
		if(HeroLife>0){
			PlayerMovement.Life = HeroLife;
			yield return new WaitForSeconds(5);
			EnemyController.battle -= 1;
			SceneManager.LoadScene(lastScene, LoadSceneMode.Single);
		}else{
			Hero.SetTrigger("Die");
			yield return new WaitForSeconds(5);

			SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
		}
		
    }
}
