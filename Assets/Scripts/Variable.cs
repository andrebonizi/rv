﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variable : MonoBehaviour
{
    private static int num;
    private static string str;
    private static bool bol;
    

    // Start is called before the first frame update
    void Start()
    {
        num =0;
        str="";
        bol=false;
    }

    private static int GetNum(){
        return num;
    }

    private static string GetStr(){
        return str;
    }


    private static bool GetBol(){
        return bol;
    }

    private static void SetNum(int numPar){
        num = numPar;
    }

    private static void SetStr(string strPar){
        str = strPar;
    }

    private static void SetBol(bool bolPar){
        bol = bolPar;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
