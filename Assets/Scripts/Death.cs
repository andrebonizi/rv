﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    public Animator Hero;
    // Start is called before the first frame update
    void Start()
    {
        Hero.SetTrigger("Die");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
