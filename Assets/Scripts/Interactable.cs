﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Interactable : MonoBehaviour {

	//private GameObject MessageBox;
	private GameObject Interaction;
	public string Message;
	
	void Start(){
		Interaction = GameObject.FindWithTag("Interactor");
		//MessageBox = GameObject.FindWithTag("ScreenCanvas");//.transform.GetChild(2).gameObject;
		//MessageBox = PlayerMovement.MessageBox;
	}

	
	void OnCollisionEnter(Collision other){
		if(other.gameObject.CompareTag("Player")){
			Interaction.SetActive(true);
			if(gameObject.CompareTag("Get")){
				Interaction.transform.GetChild(0).gameObject.SetActive(true);
			}
			if(gameObject.CompareTag("Put")){
				Interaction.transform.GetChild(1).gameObject.SetActive(true);
			}
			if(gameObject.CompareTag("Use")){
				Interaction.transform.GetChild(2).gameObject.SetActive(true);
			}
			if(gameObject.CompareTag("Back")){
				Interaction.transform.GetChild(3).gameObject.SetActive(true);
			}
			if(gameObject.CompareTag("Finish")){
				//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = "Você conseguiu!";
				//PlayerMovement.MessageBox.SetActive(true);
				SceneLoader.LoadNextScene();
			}	
		}
	}

	void OnCollisionStay(Collision other){
		if(other.gameObject.CompareTag("Player")){
			if(Input.GetKeyDown(KeyCode.Space) && Interaction.activeSelf){
				
				if(gameObject.CompareTag("Get")){
					if(PlayerMovement.GetHolding() == "nada"){
						//PlayerMovement.HoldItem(this.name);
						this.gameObject.SetActive(false);
						//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = Message;
						
					}else{
						//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = "Você já está segurando o valor "+PlayerMovement.GetHolding()+"!";
						
					}
					Interaction.transform.GetChild(0).gameObject.SetActive(false);
					//PlayerMovement.MessageBox.SetActive(true);
				}else{
					if(gameObject.CompareTag("Put")){
						if(PlayerMovement.GetHolding() != "nada"){
							//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = "X = "+PlayerMovement.GetHolding()+";";
							Message = PlayerMovement.GetHolding();
							//PlayerMovement.DropHolding();
							//isso nao vai ficar assim
							if(Message == "num:5"){
								GameObject.FindWithTag("Door").SetActive(false);
							}
							
						}else{
							//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = "Você não tem valor para colocar na variável! X = "+Message+";";
						}
						Interaction.transform.GetChild(1).gameObject.SetActive(false);
						
					}else{
						if(gameObject.CompareTag("Use")){
							//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = Message;
							
						}else{
							if(gameObject.CompareTag("Back")){
								//PlayerMovement.MessageBox.GetComponentInChildren<Text>().text = Message;
								SceneLoader.LoadPrevScene();
							}else{
								
							}
							
						}
						
						
					}
				}
				Interaction.transform.GetChild(2).gameObject.SetActive(false);
				//PlayerMovement.MessageBox.SetActive(true);
			}
			
		}
	}

	void OnCollisionExit(Collision other){
		if(other.gameObject.CompareTag("Player")){
			Interaction.SetActive(false);
			Interaction.transform.GetChild(0).gameObject.SetActive(false);
			Interaction.transform.GetChild(1).gameObject.SetActive(false);
			Interaction.transform.GetChild(2).gameObject.SetActive(false);
			Interaction.transform.GetChild(3).gameObject.SetActive(false);
			//PlayerMovement.MessageBox.SetActive(false);
			
		}
	}
}
