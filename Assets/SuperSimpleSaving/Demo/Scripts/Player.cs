﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour {
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;

    public float rotationSpeed = 3;

    CharacterController characterController;
    Transform head;

    public Transform holoCube;

    public GameObject spawnableCube;

	// Use this for initialization
	void Start () {
        characterController = GetComponent<CharacterController>();
        head = GetComponentInChildren<Camera>().transform;
        previewCube.material = MaterialStore.Instance.GetMaterial(currentCubeMaterial);
    }

    void ClampHeadRotation()
    {
        Vector3 localEuler = head.localEulerAngles;
        if(localEuler.x > 70 && localEuler.x < 180)
        {
            localEuler.x = 70;
        }
        else if(localEuler.x < 290 && localEuler.x > 180)
        {
            localEuler.x = 290;
        }
        head.localEulerAngles = localEuler;
    }

    void Update()
    {
        transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * rotationSpeed * 10 * Time.deltaTime);
        head.Rotate(Vector3.left * Input.GetAxis("Mouse Y") * rotationSpeed * 10 * Time.deltaTime);
        ClampHeadRotation();

        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);

        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && Vector3.Distance(transform.position, hit.point) < 10)
        {
            holoCube.gameObject.SetActive(true);
            holoCube.eulerAngles = new Vector3(holoCube.eulerAngles.x, 0, holoCube.eulerAngles.z);
            holoCube.position = hit.point + (Vector3.up * 0.5f * holoCube.lossyScale.y);

            if (Input.GetMouseButtonDown(0))
            {
                SpawnCube();
            }
        }        
        else
        {
            holoCube.gameObject.SetActive(false);
        }

        RaycastHit hit2;
        if (Input.GetMouseButtonDown(1) && Physics.Raycast(ray, out hit2))
        {
            PersistentCube tmp = hit2.collider.gameObject.GetComponent<PersistentCube>();
            if (tmp != null) tmp.Destroy();
        }

        holoCube.localScale += Vector3.one * Time.deltaTime * Input.mouseScrollDelta.y * 5;
        holoCube.localScale = Mathf.Clamp(holoCube.localScale.x, 0.3f, 1.7f) * Vector3.one;

        if (Input.GetKeyDown("e"))
        {
            if (currentCubeMaterial < MaterialStore.Instance.Count - 1)
                currentCubeMaterial++;
            else
                currentCubeMaterial = 0;
            previewCube.material = MaterialStore.Instance.GetMaterial(currentCubeMaterial);
        }
        if (Input.GetKeyDown("q"))
        {
            if (currentCubeMaterial > 0)
                currentCubeMaterial--;
            else
                currentCubeMaterial = MaterialStore.Instance.Count - 1;
            previewCube.material = MaterialStore.Instance.GetMaterial(currentCubeMaterial);
        }
    }

    private int currentCubeMaterial = 0;
    [SerializeField]
    private Renderer previewCube;

    void SpawnCube()
    {
        GameObject cube = Instantiate(spawnableCube);
        cube.transform.position = holoCube.position;
        cube.transform.rotation = holoCube.rotation;
        cube.transform.localScale = holoCube.localScale;
        cube.SendMessage("SetCubeMaterial", currentCubeMaterial);
    }
}
