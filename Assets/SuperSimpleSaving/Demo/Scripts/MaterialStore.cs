﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialStore : MonoBehaviour {

    public static MaterialStore Instance { get; private set; }

    [SerializeField]
    private Material[] materials;

    public int Count { get { return materials.Length; } }

    public Material GetMaterial(int index)
    {
        if (index >= 0 && index < Count)
            return materials[index];
        else
            throw new UnityException("Out of bounds");
    }

	// Use this for initialization
	void Start () {
        Instance = this;
	}
}
