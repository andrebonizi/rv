﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentCube : PersistentTransform {
    public void SetCubeMaterial(int index)
    {
        cubeMaterial = index;
        GetComponent<Renderer>().material = MaterialStore.Instance.GetMaterial(cubeMaterial);
    }

    private int cubeMaterial;

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public override void Save(ref Record record)
    {
        base.Save(ref record);
        record.SetAttribute("material", cubeMaterial);
    }

    public override void Load(Record record)
    {
        base.Load(record);
        int tmpMaterial = (int) record.GetAttributeAsNumber("material");
        SetCubeMaterial(tmpMaterial);
    }
}
