﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

/// <summary>
/// Class used to store data for a PersistentObject
/// </summary>
public class Record {
    public const string ATTRIBUTE_ID = "id";
    public const string ATTRIBUTE_NAME = "name";
    public const string ATTRIBUTE_PARENT = "parent";
    public const string ATTRIBUTE_POSITION = "position";
    public const string ATTRIBUTE_ROTATION = "rotation";
    public const string ATTRIBUTE_SCALE = "scale";

    private XmlElement xml;

    /// <summary>
    /// The XmlElement representing the data stored in this record
    /// </summary>
    public XmlElement Xml { get { return xml; } }
    
    /// <summary>
    /// Create a new, empty record
    /// </summary>
    /// <param name="name">Name of the xml element</param>
    public Record(string name = "record")
    {
        xml = new XmlDocument().CreateElement(name);
    }

    /// <summary>
    /// Create a record from an XmlElement
    /// </summary>
    /// <param name="xml">XmlElement containing the record data</param>
    public Record(XmlElement xml)
    {
        this.xml = xml;
    }

    /// <summary>
    /// Delete data from the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    public void ClearAttribute(string name)
    {
        if (xml.HasAttribute(name)) xml.RemoveAttribute(name);
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, string value)
    {
        xml.SetAttribute(name, value);
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, int value)
    {
        xml.SetAttribute(name, value.ToString());
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, float value)
    {
        xml.SetAttribute(name, value.ToString());
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, bool value)
    {
        xml.SetAttribute(name, value.ToString());
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, Vector3 value)
    {
        string stringValue = value.ToString();
        stringValue = stringValue.Substring(1, stringValue.Length - 2);
        xml.SetAttribute(name, stringValue);
    }

    /// <summary>
    /// Add data to the record
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <param name="value">Value of the attribute</param>
    public void SetAttribute(string name, byte[] value)
    {
        xml.SetAttribute(name, Convert.ToBase64String(value));
    }

    /// <summary>
    /// Get data from the record as a string
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <returns>The data as a string</returns>
    public string GetAttribute(string name)
    {
        string value = xml.GetAttribute(name);
        return value == null ? string.Empty : value;
    }

    /// <summary>
    /// Get data from the record as a number (float)
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <returns>The data as a float</returns>
    public float GetAttributeAsNumber(string name)
    {
        string stringValue = GetAttribute(name);
        float number;
        float.TryParse(stringValue, out number);
        return number;
    }

    /// <summary>
    /// Get data from the record as a boolean value
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <returns>The data as a boolean value</returns>
    public bool GetAttributeAsBool(string name)
    {
        string stringValue = GetAttribute(name);
        bool value;
        bool.TryParse(stringValue, out value);
        return value;
    }

    /// <summary>
    /// Get data from the record as a Vector3
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <returns>The data as a Vector3</returns>
    public Vector3 GetAttributeAsVector(string name)
    {
        string[] stringValues = GetAttribute(name).Split(',');
        float[] values = new float[3];
        for(int i = 0; i < values.Length && i < stringValues.Length; i++)
            float.TryParse(stringValues[i], out values[i]);
        return new Vector3(values[0], values[1], values[2]);
    }

    /// <summary>
    /// Get data from the record as binary data
    /// </summary>
    /// <param name="name">Name of the attribute</param>
    /// <returns>The data as a byte array</returns>
    public byte[] GetAttributeAsBinary(string name)
    {
        return Convert.FromBase64String(GetAttribute(name));
    }


    public override string ToString()
    {
        return xml.OuterXml;
    }
}
