﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spawnables Library", menuName = "SimpleSave/Spawnables Library")]
public class SpawnablesLibrary : ScriptableObject {
    /// <summary>
    /// Collection of persistent, spawnable objects
    /// </summary>
    public List<PersistentObject> spawnableList;
    
    public PersistentObject Get(long id)
    {
        if (spawnableList == null) return null;
        foreach (PersistentObject spawnable in spawnableList)
        {
            if (spawnable.id == id) return spawnable;
        }
        return null;
    }
}
