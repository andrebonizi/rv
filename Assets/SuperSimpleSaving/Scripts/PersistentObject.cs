﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class that can be saved by SimpleSave
/// </summary>
public abstract class PersistentObject : MonoBehaviour {

    /// <summary>
    /// The unique id of the spawnable (Used to look up the GameObject in the SpawnablesLibrary)
    /// </summary>
    public long id;

    private void Reset()
    {
        id = System.DateTime.UtcNow.ToBinary();
    }

    /// <summary>
    /// Save the GameObject
    /// </summary>
    /// <param name="record">The record in which to save the data for this object</param>
    public abstract void Save(ref Record record);

    /// <summary>
    /// Loard the GameObject
    /// </summary>
    /// <param name="record">The record from which to load the data for this object</param>
    public abstract void Load(Record record);

    private void Awake()
    {
        SuperSimpleSaving.Instance.Register(this);
    }

    private void OnDestroy()
    {
        SuperSimpleSaving.Instance.Unregister(this);
    }
}
