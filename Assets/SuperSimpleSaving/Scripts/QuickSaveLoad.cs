﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuickSaveLoad : MonoBehaviour {

    public string quicksaveKey = "f8";
    public string quickloadKey = "f7";
    public string reloadLevel = "f5";

    public string playerPrefsKey = "quicksave";

	private void Update () {
        if (!string.IsNullOrEmpty(quickloadKey) && Input.GetKeyDown(quicksaveKey))
        {
            SuperSimpleSaving.Instance.SaveToPlayerPrefs(playerPrefsKey);
            print("Saved!");
        }
        if (!string.IsNullOrEmpty(quickloadKey) &&  Input.GetKeyDown(quickloadKey))
        {
            bool success = SuperSimpleSaving.Instance.LoadFromPlayerPrefs(playerPrefsKey);
            print(string.Format("Loading {0}!", success ? "sucessful" : "failed"));
        }
        if (!string.IsNullOrEmpty(reloadLevel) &&  Input.GetKeyDown(reloadLevel))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}
