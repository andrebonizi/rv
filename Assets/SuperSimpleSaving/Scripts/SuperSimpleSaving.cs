﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

public class SuperSimpleSaving : MonoBehaviour {
    /// <summary>
    /// Name of the root element in the xml save file
    /// </summary>
    public const string XML_ROOT_NAME = "SimpleSave";

    private static bool destroyed = false;
    private static SuperSimpleSaving instance;
    public static SuperSimpleSaving Instance {
        get
        {
            if(instance == null && !destroyed)
            {
                instance = FindObjectOfType<SuperSimpleSaving>();
                if(instance == null)
                    instance = new GameObject("SimpleSave (Singleton)").AddComponent<SuperSimpleSaving>();
                instance.persistentObjects = new HashSet<PersistentObject>();
            }
            return instance;
        }
    }

    /// <summary>
    /// Obfuscator settings for reading and writing savegame data
    /// </summary>
    public Obfuscator.Settings obfuscationSettings;

    /// <summary>
    /// Library containing all persistent, spawnable prefabs
    /// </summary>
    public SpawnablesLibrary spawnables;

    private HashSet<PersistentObject> persistentObjects;

    public bool Register(PersistentObject persistentObject)
    {
        return persistentObjects.Add(persistentObject);
    }

    public bool Unregister(PersistentObject persistentObject)
    {
        return persistentObjects.Remove(persistentObject);
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Debug.LogError("Error: Instance of SimpleSave already present.\nDisabling Component!", this);
            enabled = false;
        }
        else
        {
            if(persistentObjects == null)
                persistentObjects = new HashSet<PersistentObject>();
            instance = this;
        }
    }

    /// <summary>
    /// Create a new savegame for the current state of the game
    /// </summary>
    /// <param name="name">Name of the savegame</param>
    /// <returns>The savegame that was created</returns>
    public string Save(string name = null)
    {
        XmlDocument doc = new XmlDocument();
        XmlElement root = doc.CreateElement(XML_ROOT_NAME);
        if (name != null) root.SetAttribute("name", name);
        root.SetAttribute("created", System.DateTime.Now.ToString());
        
        foreach (PersistentObject persistentObject in persistentObjects)
        {
            Record record = new Record(persistentObject.GetType().ToString());
            record.SetAttribute(Record.ATTRIBUTE_ID, persistentObject.id.ToString());
            persistentObject.Save(ref record);
            root.AppendChild(doc.ImportNode(record.Xml, true));
        }

        doc.AppendChild(root);
        Obfuscator.settings = obfuscationSettings;
        return Obfuscator.encodeToString(doc.OuterXml);
    }

    /// <summary>
    /// Load a savegame from a string
    /// </summary>
    /// <param name="xml">The savegame as a string</param>
    /// <returns>True, if the savegame could be loaded, false, if something went wrong</returns>
    public bool Load(string xml)
    {
        Obfuscator.settings = obfuscationSettings;
        xml = Obfuscator.decodeFromString(xml);
        if (spawnables == null)
        {
            Debug.LogError("No SpawnablesLibrary referenced!", this);
            return false;
        }
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.LoadXml(xml);
        }
        catch (Exception e)
        {
            Debug.LogError("Could not load xml\nError: " + e.Message);
            return false;
        }
        if (doc.FirstChild == null || !doc.FirstChild.Name.Equals(XML_ROOT_NAME))
        {
            Debug.LogError("The root node of the xml file must be called SimpleSave!");
            return false;
        }

        foreach (PersistentObject persistentObject in persistentObjects)
            Destroy(persistentObject.gameObject);

        List<PersistentObjectRecordPair> createdObjectDataPairs = new List<PersistentObjectRecordPair>(); 

        foreach(XmlElement xmlRecord in doc.FirstChild.ChildNodes)
        {
            Record record = new Record(xmlRecord);
            string spawnableIdString = record.GetAttribute(Record.ATTRIBUTE_ID);
            if (spawnableIdString.Equals(string.Empty))
            {
                Debug.LogError("Could not create PersistentObject from Record!\nNo Attribute \"id\" found!\n" + xmlRecord.OuterXml);
                continue;
            }
            long spawnAbleId;
            long.TryParse(spawnableIdString, out spawnAbleId);
            PersistentObject spawnable = spawnables.Get(spawnAbleId);
            if (spawnable == null)
            {
                Debug.LogError("Could not create PersistentObject from Record!\nid not found!\n" + xmlRecord.OuterXml, this);
                continue;
            }
            PersistentObjectRecordPair objectDataPair = new PersistentObjectRecordPair();
            objectDataPair.persistentObject = Instantiate(spawnable);
            objectDataPair.record = record;
            createdObjectDataPairs.Add(objectDataPair);
        }

        foreach(PersistentObjectRecordPair objectDataPair in createdObjectDataPairs)
        {
            objectDataPair.persistentObject.Load(objectDataPair.record);
        }

        return true;
    }

    /// <summary>
    /// Create a new savegame for the current state of the game and store it in the player prefs
    /// </summary>
    /// <param name="key">Name of the player prefs entry</param>
    /// <param name="name">Name of the savegame</param>
    public void SaveToPlayerPrefs(string key, string name = null)
    {
        PlayerPrefs.SetString(key, Save(name));
    }

    /// <summary>
    /// Load a savegame from the player prefs
    /// </summary>
    /// <param name="key">Name of the player prefs entry</param>
    /// <returns>True, if the savegame could be loaded, false, if something went wrong</returns>
    public bool LoadFromPlayerPrefs(string key)
    {
        string savegame = PlayerPrefs.GetString(key);
        if (string.IsNullOrEmpty(savegame))
            return false;
        return Load(savegame);
    }

    private const string SAVEFILE_FOLDER_NAME = "savefiles";
    private const string SAVEFILE_FILE_ENDING = ".save";
    /// <summary>
    /// The path of the directory in which the save files will be stored
    /// </summary>
    public string SaveGameDirectory
    {
        get { return Application.persistentDataPath + Path.DirectorySeparatorChar + SAVEFILE_FOLDER_NAME + Path.DirectorySeparatorChar; }
    }

    /// <summary>
    /// Create a new savegame for the current state of the game and store it as a save file
    /// </summary>
    /// <param name="name">Name of the save file (without file ending of directory)</param>
    public void SaveToFile(string name)
    {
        if(!Directory.Exists(SaveGameDirectory)){
            Directory.CreateDirectory(SaveGameDirectory);
        }
        File.WriteAllText(SaveGameDirectory + name + SAVEFILE_FILE_ENDING, Save(name));
    }

    /// <summary>
    /// Load a savegame from a save file
    /// </summary>
    /// <param name="name">Name of the save file (without file ending of directory)</param>
    /// <returns>True, if the savegame could be loaded, false, if something went wrong</returns>
    public bool LoadFromFile(string name)
    {
        string path = SaveGameDirectory + name + SAVEFILE_FILE_ENDING;
        if (!File.Exists(path)) return false;
        return Load(File.ReadAllText(path));
    }

    /// <summary>
    /// Get a list of all the save files in the savegame folder
    /// </summary>
    /// <returns>A list containing the names (without file ending) of all save files found</returns>
    public List<string> ListSaveFiles()
    {
        List<string> filenames = new List<string>();
        if (Directory.Exists(SaveGameDirectory)) {
            foreach(string path in Directory.GetFiles(SaveGameDirectory))
            {
                if (!path.EndsWith(SAVEFILE_FILE_ENDING))
                    continue;
                string[] tmp = path.Replace(SAVEFILE_FILE_ENDING, string.Empty).Split(new char[] { Path.DirectorySeparatorChar });
                string name = tmp[tmp.Length - 1];
                filenames.Add(name);
            }
        }
        return filenames;
    }

    private struct PersistentObjectRecordPair
    {
        public PersistentObject persistentObject;
        public Record record;
    }

    private void OnDestroy()
    {
        destroyed = true;
    }
}
