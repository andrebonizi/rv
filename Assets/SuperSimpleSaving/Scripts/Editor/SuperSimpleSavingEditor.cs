﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SuperSimpleSaving))]
public class SuperSimpleSavingEditor : Editor {
    public override void OnInspectorGUI()
    {
        SuperSimpleSaving simpleSave = target as SuperSimpleSaving;
        if (simpleSave == null || simpleSave.gameObject == null) return;

        if (simpleSave.spawnables == null)
        {
            EditorGUILayout.HelpBox("No Spawnables Library referenced!", MessageType.Warning);
            if(GUILayout.Button("Create and reference")){
                SpawnablesLibrary spawnablesLibrary = ScriptableObject.CreateInstance<SpawnablesLibrary>();
                string path = AssetDatabase.GenerateUniqueAssetPath("Assets/SimpleSave/New Spawnables Library.asset");
                AssetDatabase.CreateAsset(spawnablesLibrary, path);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                simpleSave.spawnables = spawnablesLibrary;
                Selection.activeObject = spawnablesLibrary;
            }
        }
        base.OnInspectorGUI();
    }
}
