﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// Static class used to encode or decode savegame data
/// </summary>
public static class Obfuscator {
    [System.Serializable]
    
    /// <summary>
    /// Obfuscation settings
    /// </summary>
    public struct Settings
    {
        /// <summary>
        /// If true, the data will be binary inverted
        /// </summary>
        public bool invert;
        /// <summary>
        /// Additional number used to mask bytes
        /// </summary>
        public byte byteIndexOffset;
        /// <summary>
        /// Key used for XOR encryption
        /// </summary>
        public string xorKey;
    }

    /// <summary>
    /// Active obfuscator settings
    /// </summary>
    public static Settings settings;

    /// <summary>
    /// Obfuscate a given string
    /// </summary>
    /// <param name="str">The string to be obfuscated</param>
    /// <returns>The obfuscated string</returns>
    public static string encodeToString(string str)
    {
        return Encoding.Unicode.GetString(encode(str));
    }

    /// <summary>
    /// Obfuscate a given string
    /// </summary>
    /// <param name="str">The string to be obfuscated</param>
    /// <returns>The binary obfuscated content</returns>
    public static byte[] encode(string str)
    {
        str = xor(str, settings.xorKey);
        byte[] bytes = Encoding.Unicode.GetBytes(str);
        for (int i = 0; i < bytes.Length; i++)
        {
            if(settings.invert)
                bytes[i] ^= byte.MinValue;
            bytes[i] += settings.byteIndexOffset;
        }
        return bytes;
    }

    /// <summary>
    /// Reconstruct an obfuscated string
    /// </summary>
    /// <param name="str">The obfuscated string</param>
    /// <returns>The recreated string</returns>
    public static string decodeFromString(string str)
    {
        return decode(Encoding.Unicode.GetBytes(str));
    }

    /// <summary>
    /// Reconstruct obfuscated binary data
    /// </summary>
    /// <param name="bytes">The obfuscated binary data</param>
    /// <returns>The recreated string</returns>
    public static string decode(byte[] bytes)
    {
        for(int i = 0; i < bytes.Length; i++)
        {
            bytes[i] -= settings.byteIndexOffset;
            if (settings.invert)
                bytes[i] ^= byte.MinValue;
        }
        string str = Encoding.Unicode.GetString(bytes);
        return xor(str, settings.xorKey);
    }

    /// <summary>
    /// XOR encrypt/ decrypt a given string (symmetric)
    /// </summary>
    /// <param name="text">The string to be encrypted/ decrypted</param>
    /// <param name="key">The encryption/ decryption key</param>
    /// <returns>They encrypted/ decrypted string</returns>
    private static string xor(string text, string key)
    {
        if (string.IsNullOrEmpty(key))
            return text;

        var result = new StringBuilder();

        for (int c = 0; c < text.Length; c++)
            result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

        return result.ToString();
    }
}
