﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentTransform : PersistentObject {
    /// <summary>
    /// If false, the GameObject will be created in the hirarchy root when loaded
    /// </summary>
    public bool referenceParent = false;

    public override void Save(ref Record record)
    {
        record.SetAttribute(Record.ATTRIBUTE_NAME, transform.name);
        if (referenceParent) record.SetAttribute(Record.ATTRIBUTE_PARENT, GetFullName(transform.parent));
        record.SetAttribute(Record.ATTRIBUTE_POSITION, transform.position);
        record.SetAttribute(Record.ATTRIBUTE_ROTATION, transform.eulerAngles);
        record.SetAttribute(Record.ATTRIBUTE_SCALE, transform.lossyScale);
    }

    public override void Load(Record record)
    {
        transform.name = record.GetAttribute(Record.ATTRIBUTE_NAME);
        transform.position = record.GetAttributeAsVector(Record.ATTRIBUTE_POSITION);
        transform.eulerAngles = record.GetAttributeAsVector(Record.ATTRIBUTE_ROTATION);
        transform.localScale = record.GetAttributeAsVector(Record.ATTRIBUTE_SCALE);
        string parentName = record.GetAttribute(Record.ATTRIBUTE_PARENT);
        if (!parentName.Equals(string.Empty))
        {
            StartCoroutine(SetParent(parentName, record.GetAttributeAsVector(Record.ATTRIBUTE_SCALE)));
        }
    }

    private IEnumerator SetParent(string parent, Vector3 localScale)
    {
        yield return new WaitForEndOfFrame();

        GameObject referencedParent = GameObject.Find(parent);
        if (referencedParent == null)
        {
            Debug.LogError(string.Format("Could not set parent to \"{0}\"!", parent));
            gameObject.SetActive(false);
        }
        else
        {
            transform.SetParent(referencedParent.transform);
        }        
    }

    private string GetFullName(Transform transform)
    {
        if (transform == null) return string.Empty;
        return GetFullName(transform.parent) + '/' + transform.name;
    }
}
