﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextTransition : MonoBehaviour
{
    private GameObject panel;  
    private Text text;  
    public Component[] list;
    // Start is called before the first frame update
    void Start()
    {
        panel = this.gameObject;
        list = panel.GetComponentsInChildren<Text>();
        text = list[0].GetComponent<Text>();
        //the higher these values the slower
        StartCoroutine(RunFade(.2f, 2f));
        
        //StartCoroutine(FadeTextToFullAlpha(1f, GetComponent<Text>()));
        //StartCoroutine(FadeZeroToFullAlpha(1f, GetComponent<Text>()));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator RunFade(float f, float t)
{
    while (true) {
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[0].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[0].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[1].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[1].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[2].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[2].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[3].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[3].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[4].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[4].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[5].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[5].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[6].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[6].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToFullAlpha(t, list[7].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        yield return StartCoroutine(FadeTextToZeroAlpha(t, list[7].GetComponent<Text>()));
        yield return new WaitForSeconds(f);
        SceneLoader.Load("Move");
    }
}

    public IEnumerator FadeTextToFullAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 0f);
        while (i.color.a < 1.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
            yield return null;
        ;
        }
    }
 
    public IEnumerator FadeTextToZeroAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
            yield return null;
        }
    }
}
